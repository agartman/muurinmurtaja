import React, { Component } from 'react';

import './App.css';
import Block from './Block';
import {blockGenerator} from './Streams';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      numbers: []
    };

    blockGenerator().onValue(x => {
      var newState = { numbers : [...this.state.numbers, x]};
      this.setState(newState);
    });
  }

  render() {
    return (      
      <div className="App">
        {this.state.numbers.map(function(object, i){
          return (<Block key={object}/>);
      })}
      </div>
    );
  }
}

export default App;
