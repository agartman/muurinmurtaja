import Kefir from 'kefir';

export function blockGenerator() {
    let start = 0;
    return Kefir.withInterval(100, emitter => {
        if (start < 10) {
            emitter.emit(start++);   // emit a value
        } else {
            emitter.end();        // end the stream
        }
    })
}



